using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Entity2D : MonoBehaviour
{
    private int entityID;
    public EntityData entityDataGlobal;
    [System.NonSerialized]
    public EntityData entityData;
    private bool selected;
    private bool highlighted;
    private GameObject manager;
    private SelectManager selectManager;
    private PoolManager poolManager;
    private GameManager gameManager;
    private Pathfinding pathfinding;
    private List<Vector3> waypoints;
    private Vector3 nextWaypoint;
    private bool goToTarget;
    private bool ignoreOthers;
    private List<GameObject> visionTargets;
    private List<GameObject> attackTargets;
    public GameEventEntitySelect selectEvent;
    private Entity upgradeTo;

    void Awake()
    {
        this.manager = GameObject.Find("Manager");
        this.selectManager = manager.GetComponent<SelectManager>();
        this.poolManager = manager.GetComponent<PoolManager>();
        this.pathfinding = manager.GetComponent<Pathfinding>();
    }

    void OnEnable()
    {
        this.entityData = Instantiate(this.entityDataGlobal);
        this.entityData.name = this.entityDataGlobal.name;
        this.selected = false;
        this.highlighted = false;
        this.waypoints = new List<Vector3>();
        this.goToTarget = false;
        this.ignoreOthers = false;
        this.visionTargets = new List<GameObject>();
        this.attackTargets = new List<GameObject>();
    }

    void Update()
    {
        if (this.goToTarget)
        {
            if ((this.nextWaypoint - this.transform.position).magnitude < 0.1)
            {
                this.waypoints.Remove(this.nextWaypoint);
                if (waypoints.Count > 0)
                {
                    this.nextWaypoint = this.waypoints[0];
                    this.setDirection();
                }
                else
                {
                    if (this.ignoreOthers && this.visionTargets.Count > 0)
                    {
                        this.createPathToTarget(this.visionTargets[0].transform.position);
                        //this.startPathing();
                        this.ignoreOthers = false;
                    }
                    else
                    {
                        this.stopPathing();
                    }
                }
            }
        }
        // Right click: go to point
        if (this.selected && Input.GetMouseButtonDown(1))
        {
            Vector2 worldMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            this.stopPathing();
            this.createPathToTarget(worldMousePosition);
            this.startPathing();
            this.ignoreOthers = true;
        }

    }

    /* Getters and Setters */

    public int getID()
    {
        return this.entityID;
    }

    public void setID(int id)
    {
        this.entityID = id;
    }

    public void addVisionTarget(GameObject entity)
    {
        this.visionTargets.Add(entity);
    }

    public List<GameObject> getVisionTargets()
    {
        return this.visionTargets;
    }

    public bool removeVisionTarget(GameObject entity)
    {
        return this.visionTargets.Remove(entity);
    }

    public void addAttackTarget(GameObject entity)
    {
        this.attackTargets.Add(entity);
    }

    public List<GameObject> getAttackTargets()
    {
        return this.attackTargets;
    }

    public bool removeAttackTarget(GameObject entity)
    {
        return this.attackTargets.Remove(entity);
    }

    public bool getIgnoreOthers()
    {
        return this.ignoreOthers;
    }

    public void setIgnoreOthers(bool ignore)
    {
        this.ignoreOthers = ignore;
    }

    public GameObject getQueen()
    {
        return this.gameManager.queen[0].gameObject;
    }

    /* Select */

    public void highlight()
    {
        this.highlighted = true;
        this.GetComponent<SpriteRenderer>().color = this.entityData.highlightedColor;
    }

    public void unhighlight()
    {
        this.highlighted = false;
        this.GetComponent<SpriteRenderer>().color = Color.white;
    }

    public void select()
    {
        this.selected = true;
        this.GetComponent<SpriteRenderer>().color = this.entityData.selectedColor;
        this.selectEvent.Raise(this);
    }

    public void unselect()
    {
        this.selected = false;
        this.GetComponent<SpriteRenderer>().color = Color.white;
    }

    /* Pathfinding */

    public void startPathing()
    {
        if (this.waypoints.Count > 0)
        {
            this.nextWaypoint = this.waypoints[0];
            this.goToTarget = true;
            this.setDirection();
        }
        else
        {
            Debug.LogError("No path created: use createPathToTarget()");
        }
    }

    public void stopPathing()
    {
        this.waypoints.Clear();
        this.nextWaypoint = Vector3.zero;
        this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        this.goToTarget = false;
    }

    public void createPathToTarget(Vector3 target)
    {
        pathfinding.FindPathWorldSpace(this.transform.position, target, out this.waypoints);
    }

    private void setDirection()
    {
        Vector3 way = (this.nextWaypoint - this.transform.position);
        way.Normalize();
        this.GetComponent<Rigidbody2D>().velocity = way * entityData.speed;
    }

    /* Attack */

    public void attackTarget()
    {
        GameObject projectile = this.poolManager.getItem<Projectile>(Projectile.Regular, false);
        projectile.SetActive(true);
        projectile.transform.position = this.transform.position;
        projectile.GetComponent<Projectile2D>().setTarget(this.attackTargets[0].gameObject);
        if (this.entityData.enemy)
        {
            projectile.transform.tag = "projFromEnemy";
        }
        else
        {
            projectile.transform.tag = "projFromAlly";
        }

    }

    public void die()
    {
        this.gameObject.SetActive(false);
    }

    /* Upgrade types */

    public Entity getType()
    {
        return this.upgradeTo;
    }

    public void setRole(Entity e)
    {
        if (this.entityData.type == Entity.Slime)
        {
            Specializator specializator = null;
            int queue = 0;
            foreach (GameObject gos in entityData.Specializators)
            {
                Specializator s = gos.GetComponent<Specializator>();
                if (s.getQueue() <= queue)
                {
                    queue = s.getQueue();
                    specializator = s;
                }
            }
            this.upgradeTo = e;
            specializator.suscribe(this);
            //Vector3 dir = (barr - this.transform.position).normalized * entityData.speed;
            //this.GetComponent<Rigidbody2D>().velocity = dir;
        }
    }

    /* Mouse events */

    private void OnMouseOver()
    {
        if (this.entityData.selectable && !this.selected)
        {
            this.highlight();
        }
    }

    private void OnMouseExit()
    {
        if (this.entityData.selectable && this.highlighted && !this.selected)
        {
            this.unhighlight();
        }
    }

    private void OnMouseDown()
    {
        if (this.entityData.selectable && !this.selected)
        {
            this.selectManager.nextSelect();
            this.selectManager.selectEvent += unselect;
            this.selectManager.setSelected(this);
            this.select();
        }
        else if (this.entityData.selectable && this.selected)
        {
            this.selectManager.nextSelect();
        }
    }

}
