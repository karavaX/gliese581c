using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionArea2D : MonoBehaviour
{
    private Entity2D entity;
    private CircleCollider2D area;

    // Start is called before the first frame update
    void Awake()
    {
        this.entity = this.GetComponentInParent<Entity2D>();
        this.area = this.GetComponent<CircleCollider2D>();
        this.area.radius = entity.entityDataGlobal.visionRange;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // Check only for enemies
        if ((this.entity.transform.tag == "ally" && other.transform.tag == "enemy") || (this.entity.transform.tag == "enemy" && other.transform.tag == "ally") || (this.entity.transform.tag == "enemy" && other.transform.tag == "tower") || (this.entity.transform.tag == "ally" && other.transform.tag == "nexusEnemy") || (this.entity.transform.tag == "enemy" && other.transform.tag == "nexusAlly"))
        {
            // Get the enemy position
            Vector3 target = other.transform.position;
            // Add it to in vision targets
            this.entity.addVisionTarget(other.gameObject);
            // If first in vision, create the path and go
            if (this.entity.getVisionTargets().Count == 1 && !this.entity.getIgnoreOthers())
            {
                this.entity.createPathToTarget(target);
                this.entity.startPathing();
            }
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        // TODO update path
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        // Check only for rivals
        if ((this.entity.transform.tag == "ally" && other.transform.tag == "enemy") || (this.entity.transform.tag == "enemy" && other.transform.tag == "ally") || (this.entity.transform.tag == "enemy" && other.transform.tag == "tower") || (this.entity.transform.tag == "ally" && other.transform.tag == "nexusEnemy") || (this.entity.transform.tag == "enemy" && other.transform.tag == "nexusAlly"))
        {
            // Retrieve vision targets list
            List<GameObject> targets = entity.getVisionTargets();
            Entity2D otherEntity = other.GetComponent<Entity2D>();
            // Check if greater
            if (targets.Count > 0)
            {
                // Chek if the target that came out is the same as the seeking
                if (otherEntity.getID() == targets[0].GetComponent<Entity2D>().getID() && !this.entity.getIgnoreOthers())
                {
                    // Stop moving and clear waypoints
                    this.entity.stopPathing();
                    this.entity.removeVisionTarget(other.gameObject);
                    // If there's others follow the first
                    if (targets.Count > 0)
                    {
                        this.entity.createPathToTarget(targets[0].transform.position);
                        this.entity.startPathing();
                    }
                }
                else
                {
                    // Just remove this mf
                    this.entity.removeVisionTarget(other.gameObject);
                }
            }
            else
            {
                if (entity.entityData.type == Entity.Patriot || entity.entityData.type == Entity.Democrat || entity.entityData.type == Entity.Redneck || entity.entityData.type == Entity.Karen)
                {
                    this.entity.createPathToTarget(this.entity.getQueen().transform.position);
                    this.entity.startPathing();
                }
            }
        }
    }
}
