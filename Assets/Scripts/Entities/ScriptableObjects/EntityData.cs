using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EntityData : ScriptableObject
{
    public Entity type;
    public bool enemy;
    public Sprite image;
    public bool selectable;
    public Color highlightedColor;
    public Color selectedColor;
    public string description;
    public int health;
    public float speed;
    public float attackDamage;
    public float attackCooldown;
    public float attackRange;
    public float visionRange;
    public GameObject[] Specializators;
}

public enum Entity { Slime, Tank, Distance, Explorer, Patriot, Redneck, Democrat, Karen,Queen };