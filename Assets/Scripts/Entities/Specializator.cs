using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Specializator : MonoBehaviour
{
    private Queue<Entity2D> entities=new Queue<Entity2D>();
    private bool free;
    public EntityData Distance;
    public EntityData Explorer;
    public EntityData Tank;
    // Start is called before the first frame update
    void awake()
    {
        this.entities = new Queue<Entity2D>();
        this.free = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(this.free && entities.Count > 0)
        {
            specializate(entities.Peek());
        }
    }
    private void specializate(Entity2D e)
    {
        this.free = false;
        // TODO: CHANGE THE SCRIPTABLEOBJECT 
        Vector3 v3 = this.transform.position - e.transform.position;
        v3.Normalize();
        e.GetComponent<Rigidbody2D>().velocity = v3 * e.entityData.speed;


    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

        if(collision.gameObject == entities.Peek().gameObject)
        {
            switch (entities.Peek().getType())
            {
                case Entity.Tank:
                    entities.Peek().entityData = this.Tank;
                    break;
                case Entity.Distance:
                    entities.Peek().entityData = this.Distance;
                    break;
                case Entity.Explorer:
                    entities.Peek().entityData = this.Explorer;
                    break;
                default:
                    entities.Peek().entityData = this.Tank;
                    break;
            }
        }
        entities.Dequeue();
        this.free = true;
    }
    public void suscribe(Entity2D e)
    {
        if(e.entityData.type==Entity.Slime)
        {
            this.entities.Enqueue(e);
        }
    }
    public int getQueue()
    {
        return this.entities.Count;
    }
}
