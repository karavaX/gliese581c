using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackArea2D : MonoBehaviour
{
    protected Entity2D entity;
    protected CircleCollider2D area;

    // Start is called before the first frame update
    void Start()
    {
        this.entity = this.GetComponentInParent<Entity2D>();
        this.area = this.GetComponent<CircleCollider2D>();
        area.radius = entity.entityDataGlobal.attackRange;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void attack()
    {
        entity.attackTarget();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if ((this.entity.transform.tag == "ally" && other.transform.tag == "enemy") || (this.entity.transform.tag == "enemy" && other.transform.tag == "ally") || (this.entity.transform.tag == "enemy" && other.transform.tag == "tower") || (this.entity.transform.tag == "ally" && other.transform.tag == "nexusEnemy") || (this.entity.transform.tag == "enemy" && other.transform.tag == "nexusAlly"))
        {
            // Add target
            entity.addAttackTarget(other.gameObject);
            // If the first, attack
            if (entity.getAttackTargets().Count == 1)
            {
                // Stop moving
                if (!this.entity.getIgnoreOthers())
                {
                    entity.stopPathing();
                }
                // Attack the first in vision target
                InvokeRepeating("attack", entity.entityData.attackCooldown, entity.entityData.attackCooldown);
            }
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
         if ((this.entity.transform.tag == "ally" && other.transform.tag == "enemy") || (this.entity.transform.tag == "enemy" && other.transform.tag == "ally") || (this.entity.transform.tag == "enemy" && other.transform.tag == "tower") || (this.entity.transform.tag == "ally" && other.transform.tag == "nexusEnemy") || (this.entity.transform.tag == "enemy" && other.transform.tag == "nexusAlly"))
        {
            // If the first, attack
            if (entity.getAttackTargets().Count > 0)
            {
                // Stop moving
                if (!this.entity.getIgnoreOthers())
                {
                    entity.stopPathing();
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if ((this.entity.transform.tag == "ally" && other.transform.tag == "enemy") || (this.entity.transform.tag == "enemy" && other.transform.tag == "ally") || (this.entity.transform.tag == "enemy" && other.transform.tag == "tower") || (this.entity.transform.tag == "enemy" && other.transform.tag == "nexusAlly"))
        {
            List<GameObject> targets = this.entity.getAttackTargets();
            Entity2D otherEntity = other.GetComponent<Entity2D>();
            // Check wtf came out
            if (otherEntity.getID() == targets[0].GetComponent<Entity2D>().getID())
            {
                CancelInvoke();
                // Remove target
                entity.removeAttackTarget(other.gameObject);
                if (this.entity.getAttackTargets().Count > 0)
                {
                    // Attack the first in vision target
                    InvokeRepeating("attack", 0.5f, 0.5f);
                }
                else
                {
                    if (!this.entity.getIgnoreOthers())
                    {
                        // Retrieve vision targets list
                        List<GameObject> vtargets = entity.getVisionTargets();
                        if (vtargets.Count > 0)
                        {
                            entity.createPathToTarget(vtargets[0].transform.position);
                            entity.startPathing();
                        }
                    }
                }
            }
            else
            {
                // Just remove this mf
                entity.removeAttackTarget(other.gameObject);
            }
        }
    }

}
