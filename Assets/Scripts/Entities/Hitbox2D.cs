using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox2D : MonoBehaviour
{
    private Entity2D entity;

    void Awake()
    {
        this.entity = this.GetComponentInParent<Entity2D>();
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if ((other.transform.tag == "projFromEnemy" && !this.entity.entityData.enemy) || (other.transform.tag == "projFromAlly" && this.entity.entityData.enemy))
        {
            other.GetComponent<Projectile2D>().setTarget(null);
            this.entity.entityData.health -= other.GetComponent<Projectile2D>().projectileData.damage;
            if (this.entity.entityData.health <= 0)
            {
                this.entity.die();
            }
            other.gameObject.SetActive(false);
        }
    }
}
