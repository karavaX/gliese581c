using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ProjectileData : ScriptableObject
{
    public Projectile type;
    public int damage;
    public float speed;
    public float Low;
}

public enum Projectile { Regular, Slow };