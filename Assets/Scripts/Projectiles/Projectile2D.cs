using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile2D : MonoBehaviour
{
    public ProjectileData projectileDataGlobal;
    [System.NonSerialized]
    public ProjectileData projectileData;
    private GameObject target;

    void OnEnable()
    {
        this.projectileData = Instantiate(this.projectileDataGlobal);
        this.projectileData.name = this.projectileDataGlobal.name;
    }

    void Update()
    {
        if (this.target.activeSelf)
        {
            this.transform.GetComponent<Rigidbody2D>().velocity = (this.target.transform.position - this.transform.position).normalized * projectileData.speed;
        }
        else
        {
            this.gameObject.SetActive(false);
        }

    }

    /* Getters and Setters */

    public void setTarget(GameObject target)
    {
        this.target = target;
    }

}
