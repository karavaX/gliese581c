using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GameData : ScriptableObject
{
    public Level levelName;
    public int playTime;
    public int currentSlimes;
    public int maxSlimes;
    public int currentEnemies;
    public int round;
    public float roundDelay;
    public int maxRounds;
}

public enum Level { Kaim, Halley, Serenity };
