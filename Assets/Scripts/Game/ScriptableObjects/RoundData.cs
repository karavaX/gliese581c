using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class RoundData : ScriptableObject
{
    // The enemy count will increase by the multiplier
    public int slimes;
    public int patriots;
    public int rednecks;
    public int democrats;
    public int karen;
    public float entityDelay;
    public float slimeDelay;
}
