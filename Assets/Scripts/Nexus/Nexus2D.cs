using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Nexus2D : MonoBehaviour
{
    public NexusData nexusDataGlobal;
    [System.NonSerialized]
    public NexusData nexusData;
    public RoundData roundDataGlobal;
    [System.NonSerialized]
    public RoundData roundData;
    public List<Position> spawnAt;
    private GameObject manager;
    private PoolManager poolManager;
    private GameManager gameManager;

    void Awake()
    {
        this.manager = GameObject.Find("Manager");
        this.poolManager = manager.GetComponent<PoolManager>();
        this.gameManager = manager.GetComponent<GameManager>();
        this.roundData = Instantiate(this.roundDataGlobal);
        this.nexusData = Instantiate(this.nexusDataGlobal);
    }

    public void startRound(int round)
    {
        if (this.nexusData.type == Nexus.Queen)
        {
            StartCoroutine(this.spawnSlimes(round));
        }
        else if (this.nexusData.type == Nexus.USSIraq)
        {
            StartCoroutine(this.spawnRound(round));
        }
    }

    IEnumerator spawnSlimes(int round)
    {
        int entityCount = 0;
        // Calculate entity count
        entityCount += this.roundData.slimes * round;
        // Start spawning
        while (entityCount > 0)
        {
            yield return new WaitForSeconds(this.roundData.slimeDelay);
            if (this.gameManager.gameData.currentSlimes < this.gameManager.gameData.maxSlimes)
            {
                this.spawnSlime();
                this.gameManager.gameData.currentSlimes++;
                entityCount--;
            }
        }
    }

    IEnumerator spawnRound(int round)
    {
        int entityCount = 0;
        // Calculate entity count
        entityCount += this.roundData.patriots * round;
        entityCount += this.roundData.rednecks * round;
        entityCount += this.roundData.democrats * round;
        entityCount += this.roundData.karen * round;
        yield return new WaitForSeconds(10f);
        // Start spawning
        while (entityCount > 0)
        {
            yield return new WaitForSeconds(this.roundData.entityDelay);
            this.spawnEnemy();
            this.gameManager.gameData.currentEnemies++;
            entityCount--;
        }
        this.roundData = Instantiate(this.roundDataGlobal);
    }

    void spawnSlime()
    {
        GameObject slime = null;
        Vector3 posSlime = Vector3.zero;
        int rand = Random.Range(0, this.spawnAt.Count);
        switch (this.spawnAt[rand])
        {
            case Position.Up:
                posSlime = new Vector3(this.transform.position.x, this.transform.position.y + this.transform.localScale.y * 2, 0);
                break;
            case Position.Down:
                posSlime = new Vector3(this.transform.position.x, this.transform.position.y - this.transform.localScale.y * 2, 0);
                break;
            case Position.Left:
                posSlime = new Vector3(this.transform.position.x - this.transform.localScale.x * 2, this.transform.position.y, 0);
                break;
            case Position.Right:
                posSlime = new Vector3(this.transform.position.x + this.transform.localScale.x * 2, this.transform.position.y, 0);
                break;
        }
        slime = this.poolManager.getItem<Entity>(Entity.Slime, false);
        slime.SetActive(true);
        slime.transform.position = posSlime;
        //slime.GetComponent<Entity2D>().createPathToTarget();
    }

    void spawnEnemy()
    {
        GameObject enemy = null;
        Vector3 posEnemy = Vector3.zero;
        if (this.roundData.patriots > 0)
        {
            enemy = this.poolManager.getItem<Entity>(Entity.Patriot, false);
            this.roundData.patriots--;
        }
        else if (this.roundData.rednecks > 0)
        {
            enemy = this.poolManager.getItem<Entity>(Entity.Redneck, false);
            this.roundData.rednecks--;
        }
        else if (this.roundData.democrats > 0)
        {
            enemy = this.poolManager.getItem<Entity>(Entity.Democrat, false);
            this.roundData.democrats--;
        }
        else if (this.roundData.karen > 0)
        {
            enemy = this.poolManager.getItem<Entity>(Entity.Karen, false);
            this.roundData.karen--;
        }
        int rand = Random.Range(0, this.spawnAt.Count);
        switch (this.spawnAt[rand])
        {
            case Position.Up:
                posEnemy = new Vector3(this.transform.position.x, this.transform.position.y + this.transform.localScale.y * 2.5f, 0);
                break;
            case Position.Down:
                posEnemy = new Vector3(this.transform.position.x, this.transform.position.y - this.transform.localScale.y * 2.5f, 0);
                break;
            case Position.Left:
                posEnemy = new Vector3(this.transform.position.x - this.transform.localScale.x * 2.5f, this.transform.position.y, 0);
                break;
            case Position.Right:
                posEnemy = new Vector3(this.transform.position.x + this.transform.localScale.x * 2.5f, this.transform.position.y, 0);
                break;
        }
        enemy.SetActive(true);
        enemy.transform.position = posEnemy;
        enemy.GetComponent<Entity2D>().createPathToTarget(this.gameManager.queen[0].transform.position);
        enemy.GetComponent<Entity2D>().startPathing();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.transform.tag == "projFromEnemy" && this.nexusData.type == Nexus.Queen) {
            other.GetComponent<Projectile2D>().setTarget(null);
            this.nexusData.health -= other.GetComponent<Projectile2D>().projectileData.damage;
            if (this.nexusData.health <= 0) {
                SceneManager.LoadScene("lose");
            }
            other.gameObject.SetActive(false);
        } else if (other.transform.tag == "projFromAlly" && this.nexusData.type == Nexus.USSIraq) {
            other.GetComponent<Projectile2D>().setTarget(null);
            this.nexusData.health -= other.GetComponent<Projectile2D>().projectileData.damage;
            if (this.nexusData.health <= 0) {
                SceneManager.LoadScene("win");
            }
            other.gameObject.SetActive(false);
        }
    }

}
