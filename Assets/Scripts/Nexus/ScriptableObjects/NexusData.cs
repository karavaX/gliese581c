using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class NexusData : ScriptableObject
{
    public Nexus type;
    public int health;
}

public enum Nexus { Queen, USSIraq }
public enum Position { Up, Down, Left, Right }