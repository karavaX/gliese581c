using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class gridcontroller : MonoBehaviour
{
    private Grid grid;
    private int id_dimensions, id_Edificio;
    public List<GameObject> building;
    public List<GameObject> towers;
    public bool activar;
    public Tilemap interactiveMap = null;
    public List<Tile> hoverTile = null;
    //public Tilemap pathMap = null;
    //public Tile pathTile = null;
    //public MouseControl MouseControlCamera;


    private Vector3Int previousMousePos = new Vector3Int();

    // Start is called before the first frame update
    void Start()
    {
        grid = gameObject.GetComponent<Grid>();
        
    }

    // Update is called once per frame
    void Update()
    {
        // Mouse over -> highlight tile
        if (activar)
        {
            //MouseControlCamera.drag_function = false;
            Vector3Int mousePos = GetMousePos();
            if (!mousePos.Equals(previousMousePos))
            {

                interactiveMap.SetTile(previousMousePos, null); // Remove old hoverTile
                interactiveMap.SetTile(mousePos, hoverTile[id_dimensions]);
                previousMousePos = mousePos;

            }

            // Left mouse click -> add path tile
            if (Input.GetMouseButtonDown(0))
            {
                //pathMap.SetTile(mousePos, pathTile);
                Vector3 pos = grid.CellToWorld(mousePos);
                pos.z = 0;
                if (interactiveMap.GetTile(mousePos) == hoverTile[0])
                {
                    pos.y += 1.15f;
                }
                else
                {
                    pos.y += 0.75f;
                }
                if(id_dimensions == 0)
                {
                    Instantiate(towers[id_Edificio], pos, Quaternion.identity);
                    interactiveMap.SetTile(previousMousePos, null);
                    activar = false;
                }
                else
                {
                    Instantiate(building[id_Edificio], pos, Quaternion.identity);
                    interactiveMap.SetTile(previousMousePos, null);
                    activar = false;
                }
                
                

            }

            // Right mouse click -> remove path tile
            if (Input.GetMouseButtonDown(1))
            {
                //pathMap.SetTile(mousePos, null);
            }
        }
    }

    Vector3Int GetMousePos()
    {
        Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        
        return grid.WorldToCell(mouseWorldPos);
    }

    public void construir(int id_dimensions, int id_Edificio)
    {
        if (activar == true)
        {
            activar = false;
        }
        else
        {
            activar = true;
            this.id_dimensions = id_dimensions;
            this.id_Edificio = id_Edificio;

        }
    }
}


