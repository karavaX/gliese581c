using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectManager : MonoBehaviour
{
    // Select
    public delegate void select();
    public event select selectEvent;
    private Entity2D selected;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void nextSelect()
    {
        this.selectEvent?.Invoke();
        this.selectEvent = null;
    }
 
    public void setSelected(Entity2D entity)
    {
        this.selected = entity;
    }
    public Entity2D getSelected()
    {
        return this.selected;
    }

}
