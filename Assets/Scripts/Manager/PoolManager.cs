using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    private static int entityID;
    public string poolPrefix;
    public List<GameObject> entities;
    public int entityCount;
    public List<GameObject> projectiles;
    public int projectileCount;
    private List<GameObject> pools;

    private void Start()
    {
        // Initiate stuff
        this.pools = new List<GameObject>();
        // Entites
        foreach (GameObject entity in this.entities)
        {
            GameObject pool = new GameObject(this.poolPrefix + entity.GetComponent<Entity2D>().entityDataGlobal.type);
            pool.transform.SetParent(this.transform);
            this.pools.Add(pool);
            for (int j = 0; j < this.entityCount; j++)
            {
                GameObject go = Instantiate(entity, Vector3.zero, Quaternion.identity);
                go.transform.SetParent(pool.transform);
                go.GetComponent<Entity2D>().setID(entityID);
                go.SetActive(false);
                entityID++;
            }
        }
        // Projectiles
        foreach (GameObject projectile in this.projectiles)
        {
            GameObject pool = new GameObject(this.poolPrefix + projectile.GetComponent<Projectile2D>().projectileDataGlobal.type);
            pool.transform.SetParent(this.transform);
            this.pools.Add(pool);
            for (int j = 0; j < this.projectileCount; j++)
            {
                GameObject go = Instantiate(projectile, Vector3.zero, Quaternion.identity);
                go.transform.SetParent(pool.transform);
                go.SetActive(false);
            }
        }
        // Add new pools below
    }

    public GameObject getItem<T>(T type, bool active)
    {
        foreach (GameObject pool in this.pools)
        {
            if (pool.transform.name == this.poolPrefix + type)
            {
                foreach (Transform child in pool.transform)
                {
                    if (child.gameObject.activeSelf == active)
                    {
                        return child.gameObject;
                    }
                }
                Debug.LogError("No available items on '" + pool.transform.name + "'");
                return null;
            }
        }
        Debug.LogError("Pool of type '" + type + "' not found");
        return null;
    }

}
