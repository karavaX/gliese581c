using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameData gameData;
    public GameEventRound gameEventRound;
    public List<Nexus2D> queen;
    public List<Nexus2D> ussIraq;
    private bool allRoundsFired;

    void Awake()
    {
        // Reset stats
        this.gameData.playTime = 0;
        this.gameData.currentSlimes = 0;
        this.gameData.currentEnemies = 0;
        this.gameData.round = 1;
        // Set other
        this.allRoundsFired = false;
    }

    void Start() {
        // Start coroutines
        StartCoroutine(playTime());
        StartCoroutine(timeRound());
    }

    void Update()
    {
        if (this.allRoundsFired && this.gameData.currentEnemies == 0) {
            SceneManager.LoadScene("win");
        }
    }

    IEnumerator playTime() {
        while (true) {
            yield return new WaitForSeconds(1f);
            this.gameData.playTime++;
        }
    }

    IEnumerator timeRound() {
        while (this.gameData.round <= this.gameData.maxRounds) {
            this.gameEventRound.Raise(this.gameData.round);
            yield return new WaitForSeconds(this.gameData.roundDelay);
            this.gameData.round++;
        }
        this.allRoundsFired = true;
    }

}
