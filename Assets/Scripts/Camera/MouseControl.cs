using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class MouseControl : MonoBehaviour
{
    private Vector3 Origin;
    private Vector3 Diff;
    private Vector3 ResetCamera;
    private bool drag;
    public bool drag_function;
    private Camera cam;
    private float targetZoom;
    private float zoomFactor = 3f;
    private float zoomLerpSpeed = 10;
    public float max_zoom = 15f;
    public float min_zoom = 1f;
    public float sensibility_movement = 0.2f;
    private float sensibility_SL_movement;
    
    private float screenOffset = 0.005f;
    public bool wasd;
    public Vector3 top_left;
    public Vector3 bot_right;

    // Start is called before the first frame update
    void Start()
    {
        this.drag = false;
        drag = false;
        this.ResetCamera = Camera.main.transform.position;
        cam = Camera.main;
        targetZoom = cam.orthographicSize;
        wasd = false;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        /**
         *  CAMERA ZOOM BY SCROLL 
         */

        float scrollData = Input.GetAxis("Mouse ScrollWheel");
        targetZoom -= scrollData * zoomFactor;
        targetZoom = Mathf.Clamp(targetZoom, min_zoom, max_zoom);
        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, targetZoom, Time.deltaTime * zoomLerpSpeed);
        
        /**
         * CAMERA MOVEMENT [DRAG] (Disactivated right now.)
        */
        if (drag_function)
        {
            if (Input.GetMouseButton(0))
            {
                Diff = (cam.ScreenToWorldPoint(Input.mousePosition)) - cam.transform.position;
                if (!drag)
                {
                    drag = true;
                    Origin = cam.ScreenToWorldPoint(Input.mousePosition);
                }

            }
            else
            {
                drag = false;
            }
            if (drag)
            {
                cam.transform.position = Origin - Diff;
            }
        }
        /**
         * 
         * CAMERA MOVEMENT [DIRECTION KEYS] (Optional, activable). 
         */
        if (wasd)
        {
            if (Input.GetKey(KeyCode.W))
            {
                cam.transform.position = new Vector3(cam.transform.position.x, cam.transform.position.y + sensibility_movement, cam.transform.position.z);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                cam.transform.position = new Vector3(cam.transform.position.x, cam.transform.position.y - sensibility_movement, cam.transform.position.z);
            }
            if (Input.GetKey(KeyCode.A))
            {
                cam.transform.position = new Vector3(cam.transform.position.x - sensibility_movement, cam.transform.position.y, cam.transform.position.z);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                cam.transform.position = new Vector3(cam.transform.position.x + sensibility_movement, cam.transform.position.y, cam.transform.position.z);

            }
        }
        

        /**
         * 
         * CAMERA MOVEMENT [MOUSE AT CAMERA'S BOUND]
         * 
         */
        sensibility_SL_movement = 25f * (targetZoom / max_zoom);
        Vector3 v = new Vector3();
        if (Input.mousePosition.x < Screen.width * screenOffset)
        {
           v.x -= sensibility_SL_movement;
            
        }else if(Input.mousePosition.x > Screen.width - (Screen.width * screenOffset))
        {
            v.x += sensibility_SL_movement;  
        }
        if (Input.mousePosition.y < Screen.height * screenOffset)
        {
           
                v.y -= sensibility_SL_movement;
            
            
        }
        else if (Input.mousePosition.y > Screen.height - (Screen.height * screenOffset))
        {
           
                v.y += sensibility_SL_movement;
            
        }
        if((transform.position+v*Time.deltaTime).x>=this.top_left.x && (transform.position + v * Time.deltaTime).x <= this.bot_right.x && (transform.position + v * Time.deltaTime).y <= this.top_left.y && (transform.position + v * Time.deltaTime).y >= this.bot_right.y)
            transform.position += v * Time.deltaTime;
    }
}
