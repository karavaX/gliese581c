using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{

    private void Awake() {
        Destroy(GameObject.Find("Music"));
    }

    public void returnToMenu() {
        SceneManager.LoadScene("menu");
    }
}
