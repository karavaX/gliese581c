using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class OptionsData : ScriptableObject
{
    public int trackSelected;
    public AudioClip[] tracks;
    public float musicVolume=1f;
}
