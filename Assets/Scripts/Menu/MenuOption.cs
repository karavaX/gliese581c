using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuOption : MonoBehaviour
{
    public Color baseColor;
    public Color highlightColor;
    private TMPro.TextMeshProUGUI text;
    private float textSize;
    // Start is called before the first frame update
    void Awake()
    {
        this.text = this.transform.GetComponentInChildren<TMPro.TextMeshProUGUI>();
        this.textSize = this.text.fontSize;
    }

    public void highlight() {
        this.text.color = this.highlightColor;
        this.text.fontSize = this.textSize * 1.1f;
    }

    public void unhighlight() {
        this.text.color = this.baseColor;
        this.text.fontSize = this.textSize;
    }
}
