using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public OptionsData menuOptions;
    public AudioSource audioSource;
    public Canvas main;
    public Canvas play;
    public Canvas options;
    private Slider volumeSlider;
    private TMPro.TMP_Dropdown trackSelect;

    void Awake()
    {
        // Set audio
        this.audioSource.volume = this.menuOptions.musicVolume;
        this.audioSource.clip = this.menuOptions.tracks[this.menuOptions.trackSelected];
        this.audioSource.Play();
        // Other
        this.volumeSlider = this.options.GetComponentInChildren<Slider>();
        this.trackSelect = this.options.GetComponentInChildren<TMPro.TMP_Dropdown>();
        // Maintain audio
        DontDestroyOnLoad(this.audioSource.gameObject);
    }

    public void playGame()
    {
        this.main.gameObject.SetActive(false);
        this.play.gameObject.SetActive(true);
        this.options.gameObject.SetActive(false);
    }

    public void playGameScene(int i)
    {
        switch (i)
        {
            case 0:
                SceneManager.LoadScene("kaim");
                break;
            case 1:
                SceneManager.LoadScene("halley");
                break;
            case 2:
                SceneManager.LoadScene("serenity");
                break;
        }
    }

    public void showMain()
    {
        this.main.gameObject.SetActive(true);
        this.play.gameObject.SetActive(false);
        this.options.gameObject.SetActive(false);
    }

    public void showOptions()
    {
        this.main.gameObject.SetActive(true);
        this.play.gameObject.SetActive(false);
        this.options.gameObject.SetActive(true);
        // Set options
        this.volumeSlider.normalizedValue = this.menuOptions.musicVolume;
        this.trackSelect.value = this.menuOptions.trackSelected;
    }

    public void setVolume(float v)
    {
        this.menuOptions.musicVolume = v;
        this.audioSource.volume = v;
    }

    public void setTrack(int o)
    {
        this.audioSource.Stop();
        this.menuOptions.trackSelected = o;
        this.audioSource.clip = this.menuOptions.tracks[o];
        this.audioSource.Play();
    }

    public void exit()
    {
        // Only in editor!
        UnityEditor.EditorApplication.isPlaying = false;
        // lmao
        Application.Quit();
    }
}
