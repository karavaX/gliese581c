using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower2D : MonoBehaviour
{
    public TowerData torredisparoGlobal;
    [System.NonSerialized]
    public TowerData torredisparo;
    public List<GameObject> enemies;
    private GameObject manager;
    private PoolManager poolManager;

    private void Awake()
    {
        this.manager = GameObject.Find("Manager");
        this.poolManager = manager.GetComponent<PoolManager>();
    }

    void OnEnable() {
        this.torredisparo = Instantiate(this.torredisparoGlobal);
        this.torredisparo.name = this.torredisparoGlobal.name;
    }

    void Start()
    {
        if (torredisparo.tipo == Tower.Regular)
        {
            InvokeRepeating("Disparo", 05f, 0.5f);
        }else if (torredisparo.tipo == Tower.Acid)
        {
            InvokeRepeating("Debilitar", 0.5f, 0.5f);
        }
    }

    void Update()
    {
        
    }



    public void Disparo()
    {
        if (enemies.Count > 0)
        {
            GameObject projectile = this.poolManager.getItem<Projectile>(Projectile.Regular, false);
            projectile.SetActive(true);
            projectile.transform.position = this.transform.position;
            projectile.GetComponent<Projectile2D>().setTarget(this.enemies[0].gameObject);
            projectile.transform.tag = "projFromAlly";
        }
        
    }
    public void Debilitar()
    {
        for (int i = 0; i < enemies.Count; ++i)
        {
            enemies[i].GetComponent<Entity2D>().entityData.health -= torredisparo.AcidDamage;
            if (enemies[i].GetComponent<Entity2D>().entityData.health <= 0)
            {
                Destroy(enemies[i].gameObject);
            }

        }
    }
       

    public void AddEnemy(GameObject enemy)
    {
        enemies.Add(enemy);
        if(torredisparo.tipo  == Tower.Slowness)
        {
            
        }
    }

    public void RemoveEnemy(GameObject enemy)
    {
        enemies.Remove(enemy);
    }

}
