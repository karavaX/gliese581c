using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TowerData : ScriptableObject
{
    public Tower tipo;
    public int Helath;
    public float FireRate;
    public int MaxTargets;
    public int AcidDamage;
}

public enum Tower { Regular, Slowness, Acid };