using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerVision2D : MonoBehaviour
{
    


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "enemy")
        {
            this.transform.parent.GetComponent<Tower2D>().AddEnemy(collision.gameObject);
        }
      
    }

    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.tag == "enemy")
        {
            this.transform.parent.GetComponent<Tower2D>().RemoveEnemy(collision.gameObject);
        }
        if(collision.tag == "fire")
        {
            collision.gameObject.SetActive(false);
        }
      
    }
}
