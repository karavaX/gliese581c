using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.WSA;

public class UIDisplay : MonoBehaviour
{
    public GameObject grid;
    gridcontroller controller;
    private Entity2D selected;
    private bool displayed_element = false;
    private AudioSource ab;
    private float musicVolume=1f;
    private int NumTowerDamage, NumTowerArea, NumTowerSpeed, NumCampFire,NumLab,SS_Lab,SS_TowerDamage,SS_TowerSpeed,SS_TowerArea,SS_Campfire;
    // Start is called before the first frame update
    void Awake()
    {
        this.ab = GameObject.Find("Music").GetComponent<AudioSource>();
        this.musicVolume = this.ab.volume;
        controller = grid.GetComponent<gridcontroller>();
        NumCampFire = 0;
        NumTowerArea = 0;
        NumTowerDamage = 0;
        NumTowerSpeed = 0;
        NumLab = 0;
        SS_TowerDamage = 0;
        SS_TowerSpeed = 0; 
        SS_TowerArea = 0;
        SS_Campfire = 0;
        SS_Lab = 0;

        transform.GetChild(0).GetChild(0).gameObject.SetActive(displayed_element);
        transform.GetChild(0).GetChild(1).gameObject.SetActive(displayed_element);
        for (int a = 0; a < transform.childCount-5; a++)
        {
            this.transform.GetChild(a).gameObject.SetActive(false);
        }

    }

    // Update is called once per frame
    void Update()
    {
        ab.volume = musicVolume;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (this.transform.GetChild(0).gameObject.activeSelf)
            {
                this.transform.GetChild(0).gameObject.SetActive(false);
            }
            if (this.transform.GetChild(1).gameObject.activeSelf)
            {
                this.transform.GetChild(1).gameObject.SetActive(false);
            }
            turn_on_of(2);
        }
        this.transform.GetChild(5).gameObject.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "TowersAvailable: "+NumTowerDamage+"\n SlimesSacrificed: "+SS_TowerDamage+" \n RequiredSlimes: "+(SS_TowerDamage-3);
        this.transform.GetChild(6).gameObject.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "TowersAvailable: " + NumTowerSpeed + "\n SlimesSacrificed: " + SS_TowerSpeed + " \n RequiredSlimes: " + (SS_TowerSpeed - 4);
        this.transform.GetChild(7).gameObject.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "TowersAvailable: " + NumTowerArea + "\n SlimesSacrificed: " + SS_TowerArea + " \n RequiredSlimes: " + (SS_TowerArea - 4);
        this.transform.GetChild(8).gameObject.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "CampfireAvailable: " + NumCampFire + "\n SlimesSacrificed: " + SS_Campfire + " \n RequiredSlimes: " + (SS_Campfire - 4);
        this.transform.GetChild(9).gameObject.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "LabsAvailable: " + NumLab + "\n SlimesSacrificed: " + SS_Lab + " \n RequiredSlimes: " + (SS_Lab - 3);
    }
    
    public void display(Entity2D t)
    {
        this.selected = t;
        if (t.entityData.type == Entity.Slime || t.entityData.type == Entity.Explorer || t.entityData.type == Entity.Distance || t.entityData.type == Entity.Tank)
        {
            turn_on_of(1);
            setInfoBlock(t);
        }
        if (t.entityData.type == Entity.Slime)
        {
            if (t.entityData.Specializators.Length > 0)
            {
                transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
                transform.GetChild(0).GetChild(1).gameObject.SetActive(true);
            }
            turn_on_of(0);
        }
       
    }
    public void Create_TowerD()
    {
        if (NumTowerDamage > 0)
        {
            controller.construir(0,0);
            NumTowerDamage--;
        }
    }
    public void Create_TowerS()
    {
        if (NumTowerSpeed > 0)
        {
            controller.construir(0,1);
            NumTowerSpeed--;
        }
    }
    public void Create_TowerA()
    {
        if (NumTowerArea > 0)
        {
            controller.construir(0,2);
            NumTowerArea--;
        }
    }
    public void Create_Lab()
    {
        if (NumLab > 0)
        {
            controller.construir(1,0);
            NumLab--;
        }
    }
    public void Create_Campfire()
    {
        if (NumCampFire > 0)
        {
            //controller.construir(1,1);
        }
    }
    public void Sum_TowerArea()
    {
        if (SS_TowerArea + 1 == 4)
        {
            SS_TowerArea = 0;
            NumTowerArea++;
        }
        else
        {
            SS_TowerArea++;
        }
        SacrificeSlime();
    }
    public void Sum_Lab()
    {
        if (SS_Lab + 1 == 3)
        {
            SS_Lab = 0;
            NumLab++;
        }
        else
        {
            SS_Lab++;
        }
        SacrificeSlime();
    }
    public void Sum_Campfire()
    {
        if (SS_Campfire + 1 == 4)
        {
            SS_Campfire = 0;
            NumCampFire++;
        }
        else
        {
            SS_Campfire++;
        }
        SacrificeSlime();
    }
    public void Sum_TowerSpeed()
    {
        if (SS_TowerSpeed + 1 == 4)
        {
            SS_TowerSpeed = 0;
            NumTowerSpeed++;
        }
        else
        {
            SS_TowerSpeed++;
        }
        SacrificeSlime();
    }
    public void Sum_TowerDamage()
    {
        if (SS_TowerDamage + 1 == 3)
        {
            SS_TowerDamage = 0;
            NumTowerDamage++;
        }
        else
        {
            SS_TowerDamage++;
        }
        SacrificeSlime();
    }
    IEnumerator animationDead()
    {
        transform.GetChild(4).gameObject.SetActive(true);
        transform.GetChild(4).transform.position = new Vector3(this.selected.transform.position.x, this.selected.transform.position.y + 1, this.selected.transform.position.z);
        transform.GetChild(4).GetComponent<Rigidbody2D>().velocity = new Vector2(0, 1);
        yield return new WaitForSeconds(1);
        transform.GetChild(4).gameObject.SetActive(false);

    }
    public void SacrificeSlime()
    {
        turn_on_of(1);
        turn_on_of(0);
        this.selected.gameObject.SetActive(false);
        this.transform.GetChild(0).GetChild(0).GetComponent<TMP_Dropdown>().value = 0;
        StartCoroutine(animationDead());
    }

    private void setInfoBlock(Entity2D t)
    {
        GameObject blockInfo = transform.GetChild(1).gameObject;
        if (t.getType() == Entity.Slime)
        {
            blockInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Normal Slime";
        }
        else
        {
            blockInfo.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Slime " + t.getType().ToString();
        }

        blockInfo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Health: " + t.entityData.health;
        blockInfo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Damage: " + t.entityData.attackDamage;
        blockInfo.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = "Speed: " + t.entityData.speed;
        blockInfo.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = "Vision Range: " + t.entityData.visionRange;
        blockInfo.transform.GetChild(5).GetComponent<TextMeshProUGUI>().text = "Attack Range: " + t.entityData.attackRange;
        blockInfo.transform.GetChild(6).GetComponent<TextMeshProUGUI>().text = "Cooldown: " + t.entityData.attackCooldown;
        if (t.entityData.image != null)
        {
            blockInfo.transform.GetChild(7).GetComponent<Image>().sprite = t.entityData.image;
        }
        

    }

    public void turn_on_of(int a)
    {
        if (this.transform.GetChild(a).gameObject.activeSelf)
        {
            this.transform.GetChild(a).gameObject.SetActive(false);
        }
        else
        {
            this.transform.GetChild(a).gameObject.SetActive(true);
        }
        
    }
    public void activate_role(int option)
    {
        if (option == 1)
        {
            selected.setRole(Entity.Tank);
        }
        else if (option == 2)
        {
            selected.setRole(Entity.Distance);
        }
        else if (option == 3)
        {
            selected.setRole(Entity.Explorer);
        }
        this.transform.GetChild(0).GetChild(0).GetComponent<TMP_Dropdown>().value = 0;
        turn_on_of(0);
    }
    public void setVolume(float a)
    {
        this.musicVolume = a;
    }
    public void backToTheMenu()
    {
        print("No Marty, debemos volver al menu!");
        SceneManager.LoadScene("menu");
    }
    public void exitGame()
    {
        UnityEditor.EditorApplication.isPlaying = false;
    }
}
