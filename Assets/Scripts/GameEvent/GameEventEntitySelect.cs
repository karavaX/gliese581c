using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/Entity2D x1")]
public class GameEventEntitySelect : GameEvent<Entity2D> {}
